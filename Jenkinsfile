pipeline {
    agent any
    stages {
       stage('build') {
          steps {
             echo 'Notify GitLab'
             updateGitlabCommitStatus name: 'build', state: 'pending'
             script {
               try {
                 echo 'Building...'
		 sh 'cp /var/lib/jenkins/envs/.env .'
                 sh 'pip3 install -r requirements.txt'
                 sh 'python3 manage.py makemigrations'
                 sh 'python3 manage.py migrate'
                 updateGitlabCommitStatus name: 'build', state: 'success'
               } catch (Exception e) {
                 echo 'Exception occurred: ' + e.toString()
                 updateGitlabCommitStatus name: 'build', state: 'failed'
                 sh 'exit 1'
               }
             }
          }
       }
       stage(test) {
           steps {
               echo 'Notify GitLab'
               updateGitlabCommitStatus name: 'test', state: 'pending'
               script {
                 try {
                   echo 'Running tests'
                   sh 'python3 manage.py test'
                   updateGitlabCommitStatus name: 'test', state: 'success'
                 } catch (Exception e) {
                   echo 'Exception occurred: ' + e.toString()
                   updateGitlabCommitStatus name: 'test', state: 'failed'
                   sh 'exit 1'
                 }
               }
           }
       }
	stage(linter) {
           steps {
               echo 'Notify GitLab'
               updateGitlabCommitStatus name: 'linter', state: 'pending'
               script {
                 try {
                   echo 'checks step goes here'
                   sh 'pycodestyle --max-line 120 .'
                   updateGitlabCommitStatus name: 'linter', state: 'success'
                 } catch (Exception e) {
                   echo 'Exception occurred: ' + e.toString()
                   updateGitlabCommitStatus name: 'linter', state: 'failed'
                   sh 'exit 1'
                 }
               }
           }
       }
	stage(security) {
           steps {
               echo 'Notify GitLab'
               updateGitlabCommitStatus name: 'security', state: 'pending'
               script {
                 try {
                   echo 'Security'
                   sh 'python3 manage.py check --deploy'
                   updateGitlabCommitStatus name: 'security', state: 'success'
                 } catch (Exception e) {
                   echo 'Exception occurred: ' + e.toString()
                   updateGitlabCommitStatus name: 'security', state: 'failed'
                   sh 'exit 1'
                 }
               }
           }
       }
       stage(deploy) {
	   environment {
		SERVER_IP = '135.181.109.40'
                PROJECT_RPATH = '/var/django'
                PROJECT_NAME = 'djangotest'
                BRANCH_NAME = "test"
		SECRET_KEY='SECRET_KEY=blablablablablablabla'
	   }
           steps {
               echo 'Notify GitLab'
               echo "${GIT_BRANCH}"
               updateGitlabCommitStatus name: 'deploy', state: 'pending'
               script {
                 try {
			echo 'Deployment on remote server'
			sh 'eval $(ssh-agent -s) && ssh-add && ssh-keyscan ${SERVER_IP} >> ~/.ssh/known_hosts && ssh -A root@${SERVER_IP} "cd ${PROJECT_RPATH}/${PROJECT_NAME} && git checkout $BRANCH_NAME && git pull"'
			sh 'ssh -A root@${SERVER_IP} "cd ${PROJECT_RPATH}/${PROJECT_NAME} && pip3 install -r requirements.txt"'
			sh 'ssh -A root@${SERVER_IP} "echo \'${SECRET_KEY}\' > ${PROJECT_RPATH}/${PROJECT_NAME}/.env"'
			sh 'ssh -A root@${SERVER_IP} "chown -R www-data: ${PROJECT_RPATH}/${PROJECT_NAME}"'
			sh 'ssh -A root@${SERVER_IP} "cd ${PROJECT_RPATH}/${PROJECT_NAME} && python3 manage.py makemigrations && python3 manage.py migrate"'
			sh 'ssh -A root@${SERVER_IP} "cd ${PROJECT_RPATH}/${PROJECT_NAME} && python3 manage.py collectstatic --noinput"'
			sh 'ssh -A root@${SERVER_IP} "chown -R www-data: ${PROJECT_RPATH}/${PROJECT_NAME}"'
			sh 'ssh -A root@${SERVER_IP} "service uwsgi reload"'
		   updateGitlabCommitStatus name: 'deploy', state: 'success'
                 } catch (Exception e) {
                   echo 'Exception occurred: ' + e.toString()
                   updateGitlabCommitStatus name: 'deploy', state: 'failed'
                   sh 'exit 1'
                 }
               }
           }
       }
    }
 }


